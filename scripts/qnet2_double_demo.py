#!/usr/bin/env python
#

# demonstration Q learning deep network with Tensorflow 2
# the game consists of a list of 4 digits, ones or zeros where the objective is to make all four digits = 1
# since there are 4 digits and each can be either 0 or 1, there are 2 ^ 4 = 16 states.  For example, [0,0,0,0], [0,0,0,1],...[1,1,1,1]
# the initial state is [0,0,0,0]
# possible actions are to toggle the value of any one of the 4 digits, so there are 4 actions
# when changing a 0 to a 1, a reward = 1 is granted.  when changing a 1 to a zero, the reward is -1
# the game is done when the state = [1,1,1,1]

# requirements are numpy and tensorflow 2

import random
import numpy as np
import tensorflow as tf
from tensorflow.keras.layers import Input, Dense, Activation
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.optimizers import SGD, Adam
from tensorflow.keras.losses import Loss, Reduction
#import matplotlib.pyplot as plt
from time import time
import datetime as dt
import math

NUM_OF_GAMES = 10
NUM_INPUTS = 4
NUM_ACTIONS = 4
BATCH_SIZE = 32
NUM_EPOCHS = 16 # number of training runs done with each step
MAX_EPSILON = 1 # probablitiy of choosing a random action rather than running the network to select an action
MIN_EPSILON = 0.01
LAMBDA = 0.0005  # how fast epsilon exponentially decays from MAX_EPSILON to MIN_EPSILON
GAMMA = 0.95    # discount factor.  Mutiply the expected reward by GAMMA when calculating the expected future reward.
LEARNING_RATE = 0.01 # use this for the SGD optimizer
HIDDEN_LAYERS_SIZE = 32
MEMORY_SIZE = 10000
NUM_MEMORY_RUNS = BATCH_SIZE * 3    # initialize the memory pool with this number of random actions
TAU = 0.08 # rate that the shadow network weights progress toward the main network weights
#STORE_PATH = '../network_outputs/Tensorboard' # for tensorboard.  Not using right now.

# seems to select the nvidia GPU even without these lines, but keep them here for future reference
#physical_devices = tf.config.experimental.list_physical_devices('GPU')
#tf.config.experimental.set_memory_growth(physical_devices[0], True)

class Game:
    def __init__(self, board_size=NUM_INPUTS):
        self.board_size = board_size
        self.board = None
        self.reset()

    def reset(self):
        self.board = np.zeros(self.board_size)
        # can choose to reset with random values, but this results in a lot of very short games
        #self.board = [random.randint(0, 1), random.randint(0, 1), random.randint(0, 1), random.randint(0, 1)]

    def play(self, index_to_change):
        # toggle the value at the index = index_to_change
        if self.board[index_to_change] == 0:
            self.board[index_to_change] = 1
            # check if the game is finished
            # this could be done in a fancy pythonic way, but this code is easier to understand
            done = True
            for i in range(NUM_INPUTS):
                if self.board[i] == 0:
                    done = False
                    break
            # decided not to reset the game here when we are done.  It gets reset in the main loop instead.
            #if done:
            #    print("GAME FINISHED, RESETTING BOARD")
            #    self.reset()

            #changed a 0 to a 1, so the reward is +1
            return 1, done
        else:
            self.board[index_to_change] = 0
            # changed a 1 to a zero, so the reward is -1 and the game is not done
            return -1, False
game = Game()

# deep Q network

# not currently using this, but it is helpful to know how to setup a custom loss function
# custom loss functions can be very tricky.  They should be differentible
# Also, since they are part of the compiling of the network, you cannot change the parameters after calling compile
# basically, you can only use fixed scalars and y_true and y_pred to calculate the loss

class CustomLoss(Loss):
    def __init__(self, gamma, reward,
                 reduction=Reduction.AUTO,
                 name='my_custom_loss'):
        super().__init__(reduction=reduction, name=name)
        self.reward = reward
        self.rewards = []
        self.rewards.append(reward)
        self.gamma = gamma

    def call(self, y_true, y_pred):
        max_y_true = tf.reduce_max(y_true, axis=1)
        expected_value = self.rewards + (self.gamma * max_y_true)
        predicted_value = tf.reduce_max(y_pred, axis=1)
        tf.print("rewards", self.rewards)
        tf.print("gamma", self.gamma)
        tf.print("max_y_true", max_y_true)
        tf.print("expected value ", expected_value)
        tf.print("predicted value: ", predicted_value)
        # tf.print("predicted value loop: ", predicted_value_loop)

        local_loss = tf.math.reduce_mean(tf.abs(expected_value - predicted_value))
        # local_loss =  tf.reduce_mean(tf.losses.mean_squared_error(expected_value, predicted_value))
        tf.print("loss: ", local_loss)
        return local_loss

# create the network
class QNetwork:
    def __init__(self, hidden_layers_size, gamma, learning_rate):

        # pick an initializer
        initializer = tf.keras.initializers.GlorotUniform()
        #initializer = tf.keras.initializers.he_normal()
        #initializer = tf.keras.initializers.Zeros()

        # use a fully connected layer with 32 nodes, taking input from 4 nodes which correspond to the input state
        # Note that input_dim = NUM_INPUTS means that the input must be have shape (1, NUM_INPUTS)
        # game.board has shape (NUM_INPUTS,] so we will use game.board.reshape([1,4]) as the input
        # define the model input, note that we can name any layer by passing it a "name" argument.
        self.model = Sequential()
        self.model.add(Dense(hidden_layers_size, input_dim=NUM_INPUTS, kernel_initializer=initializer, activation='relu'))
        self.model.add(Dense(hidden_layers_size, kernel_initializer=initializer, activation='relu'))
        # softmax makes an interesting activation function for the output.  Instead of Q values, we get probabilities
        # It seems to work, but not as well as the linear or None activation functions
        # Something to explore more
        #self.output = self.model.add(Dense(NUM_ACTIONS, kernel_initializer=initializer, activation='softmax', name='outputs'))

        #self.output = self.model.add(Dense(NUM_ACTIONS, kernel_initializer=initializer, activation='linear', name='outputs'))
        # I don't see any difference between linear activation and None.
        # Something to explore
        self.output = self.model.add(Dense(NUM_ACTIONS, kernel_initializer=initializer, activation=None, name='outputs'))

        '''
        # alternative network setup
        inputs = Input(shape=(NUM_INPUTS,), dtype='float32', name='inputs')
        # oddly enough, specifying shape = (NUM_INPUTS) means that the input must be have shape (1, NUM_INPUTS)
        # game.board has shape (NUM_INPUTS,] so we will use game.board.reshape([1,4]) as the input
        x = Dense(hidden_layers_size, activation='relu', name='dense_1')(inputs)
        x = Dense(hidden_layers_size, activation='relu', name='dense_2')(x)
        outputs = Dense(NUM_ACTIONS, name='predictions')(x)
        self.model = Model(inputs=inputs, outputs=outputs)
        '''
        # pick an optimizer
        #opt = tf.keras.optimizers.SGD(learning_rate=LEARNING_RATE)
        opt = tf.keras.optimizers.Adam()

        # we are setup to use the standard mse loss function, but it is interesting to play around with custom losses
        self.model.compile(optimizer=opt, loss='mse')
        #self.model.compile(optimizer=opt, loss=self.basic_loss_function)
        #self.model.compile(optimizer='rmsprop', loss = CustomLoss(gamma=GAMMA, reward=0.0))

        # create the second network, identical to the first, but we will not compile it
        # this netowrk is just used to store past weights from the self.model network
        self.shadow_network = Sequential([
            Dense(hidden_layers_size, input_dim=NUM_INPUTS, kernel_initializer=initializer, activation='relu'),
            Dense(hidden_layers_size, kernel_initializer=initializer, activation='relu'),
            Dense(NUM_ACTIONS, kernel_initializer=initializer, activation=None)
        ])

    # another way to choose a custom loss.  Not currently using this.
    def basic_loss_function(self, y_true, y_pred):
        #action_true = tf.math.argmax(y_true, axis=1)
        #action_pred = tf.math.argmax(y_pred, axis=1)
        #expected_value = tf.reduce_max(y_true, axis=1)
        #predicted_value = tf.reduce_max(y_pred, axis=1)
        #local_loss = tf.math.reduce_mean(tf.abs(expected_value - predicted_value))
        '''
        pred_value = tf.reduce_max(y_pred, axis=1)
        action_pred = tf.math.argmax(y_pred, axis=1)
        #true_value = tf.reduce_max(y_true, axis=1)
        true_value = []
        for i in range(BATCH_SIZE):
            true_value.append(y_true[i][action_pred[i]])
        local_loss = tf.reduce_mean(tf.abs(true_value - pred_value))
        '''
        #local_loss =  tf.reduce_mean(tf.losses.mean_squared_error(expected_value, predicted_value))
        local_loss = tf.reduce_mean(tf.square(y_true - y_pred))
        #local_loss = tf.reduce_mean(tf.losses.mean_squared_error(y_true, y_pred))
        if BATCH_SIZE == 1:
            tf.print("y_true:", y_true)
            tf.print("y_pred:", y_pred)
            #tf.print("action_true: ", action_true)
            #tf.print("action_pred: ", action_pred)
            #tf.print("expected value ", expected_value)
            #tf.print("predicted value: ", predicted_value)
            tf.print("pred_value = ", pred_value)
            tf.print("true_value = ", true_value)
            tf.print("loss: ", local_loss)
        return local_loss

qnn = QNetwork(hidden_layers_size=HIDDEN_LAYERS_SIZE, gamma=GAMMA, learning_rate=LEARNING_RATE)

#setup the memory pool
class Memory:
    def __init__(self, max_memory):
        self._max_memory = max_memory
        self._samples = []

    def add_sample(self, sample):
        self._samples.append(sample)
        if len(self._samples) > self._max_memory:
            self._samples.pop(0)

    def sample(self, no_samples):
        #if no_samples > len(self._samples):
        #    return random.sample(self._samples, len(self._samples))
        #else:
        return random.sample(self._samples, no_samples)

    @property
    def num_samples(self):
        return len(self._samples)

memory = Memory(MEMORY_SIZE)

# print out the results for every state
def print_qtable():
    print("Here are the results for all the states and actions.  This is equivalent to a Q table.")
    for i in range(2):
        for j in range(2):
            for k in range(2):
                for l in range(2):
                    b = np.array([i,j,k,l])
                    if len(np.where(b == 0)[0]) != 0:
                        single_prediction = qnn.model.predict(b.reshape([1,NUM_INPUTS]))
                        action = np.argmax(single_prediction)
                        print('board: {b}, predicted Q values: {p}, best action: {a:1d}, correct action? {s}'
                              .format(b=b,p=single_prediction,a=action,s=b[action]==0))

# initialize the memory pool using random actions
# this is a quick way to explore the Q space because it does not use any network runs
for i in range(NUM_MEMORY_RUNS):
    state = np.copy(game.board)
    # choose an action at random
    single_action = random.randint(0, 3)
    # play the game, get a reward and go to next state
    reward, game_over = game.play(single_action)  # note that this updates the game board to the next state as well as returning reward and game_over
    next_state = np.copy(game.board)
    # append the results to the memory pool
    memory.add_sample([state, single_action, reward, next_state, game_over])
    # do not want a starting state of [1,1,1,1], so reset the game when it finishes
    if game_over:
        game.reset()

r_list = [] # save rewards
c_list = []  # save costs
# for tensorboard.  Not using right now
#train_writer = tf.summary.create_file_writer(STORE_PATH + f"/DoubleQ_{dt.datetime.now().strftime('%d%m%Y%H%M')}")
game_counter = 0
steps = 0
previous_steps = steps
eps = MAX_EPSILON
for g in range(NUM_OF_GAMES):
    game_counter += 1
    print("\n\n\nnew game.  game number = ", game_counter)
    game_over = False
    game.reset()
    avg_loss = 0.0
    total_reward = 0
    train_counter = 0
    while not game_over:
        steps += 1
        # decay the eps value over time.
        eps = MIN_EPSILON + (MAX_EPSILON - MIN_EPSILON) * math.exp(-LAMBDA * steps)
        # If a random number between 0 and 1 is greater than eps, then we choose a random action
        if random.random() < eps:
            single_action = random.randint(0,3)
            #if BATCH_SIZE == 1 or game_counter > NUM_OF_GAMES - 3:
            #    print("current state: ", state, ", random single action = ", single_action)
        else:
            # run the network to get outputs based on the current state (which is game.board)
            # input to network has to have shape (1, NUM_INPUTS)
            # game.board has shape (NUM_INPUTS,] so we use game.board.reshape([1,4]) as the input
            single_prediction = qnn.model.predict(np.array(game.board).reshape([1,NUM_INPUTS]), batch_size=None)
            # from the outputs, use the biggest one to specify the action to take
            single_action = np.argmax(single_prediction)
            # when we get near the end or when testing and using BATCH_SIZE = 1, it is handy to have some data printed
            #if BATCH_SIZE == 1 or game_counter > NUM_OF_GAMES - 3:
            #    print("current state: ", state, ", single prediction: ", single_prediction, ", single_action = ", single_action)

        # save the current state
        state = np.copy(game.board)
        # play the game and get a reward and also whether the game is finished (reached state [1,1,1,1])
        # note that this updates the game board to the next state as well as returning reward and game_over
        reward, game_over = game.play(single_action)
        total_reward += reward
        next_state = np.copy(game.board)

        # append the results to the memory pool
        # note that we do not need to save the output values.
        # when the memory pool entries are used, the outputs will be based on the network weights at that time
        # not the network weights we used just now to choose this action and next state
        memory.add_sample([state, single_action, reward, next_state, game_over])

        # once we have at least BATCH_SIZE * 3 memory pool entries we can start training
        # at that point, we train once for every BATCH_SIZE new entries into the memory pool
        # Note that is an arbitrary number.  We could train after some other number of new entries into the memory pool
        # but this seems like a reasonable number.
        # Also train when the game finishes, so that we get at least one training session for each game
        # Otherwise, when the network gets well trained, we can get a lot of games that end with no training.
        if (steps % BATCH_SIZE == 0 and memory.num_samples > BATCH_SIZE * 3) or game_over:
            # Network training
            if BATCH_SIZE == 1:
                # use for testing with BATCH_SIZE = 1
                # it can be convenient to see what happens after each individual step
                # I also used BATCH_SIZE = 1 with the Zeros() initializer in the network, then printed results for each step
                # That is a great way to understand how things change from one step to the next
                batch = [[state, single_action, reward, next_state, game_over]]
            else:
                # this is the normal way to go.  Take a BATCH_SIZE number of entries, randomly selected from the memory pool
                batch = memory.sample(BATCH_SIZE)
            # each element of batch is a list of 5 entries: [current state, action, reward, next state, game_over]
            # pull them out
            current_states = []
            actions = []
            rewards = []
            next_states = []
            game_overs = []
            for values in batch:
                current_states.append(values[0])
                actions.append(values[1])
                rewards.append(values[2])
                game_overs.append(values[4])
                if values[4]:
                    # the game ended on this step and we do not want to use [1,1,1,1] as an input state, so we make the next state [0,0,0,0]
                    next_states.append(np.zeros(game.board_size))
                else:
                    # the game did not end, so we use the next state values that were in the memory pool entry
                    next_states.append(values[3])

            # run the network and get the outputs for the current state
            # note that we are going to use the action from the memory pool entry, not the action that would be selected by the output of the network run
            # that is because we want to explore the Q state based on the network weights from when this memory pool entry was appended to the memory pool
            # If instead we used the action selected by the output of the current network run, then the memory pool entry for action, reward, next state, and done would be useless
            # In that case, we might as well have just run the network with a random input state.
            # Instead, we will use the outputs from the network run to calculate the current cost function when
            # taking the action and reward and next state from the memory pool entry.
            # It is basically a way to explore more of the Q space
            current_state_prediction = qnn.model.predict(np.array(current_states).reshape([BATCH_SIZE, NUM_INPUTS]), batch_size=BATCH_SIZE)

            # print out some data to help when testing
            #if BATCH_SIZE == 1 or game_counter > NUM_OF_GAMES - 3:
            #    print("batch current_states: ", current_states)
            #    print("batch current_state_prediction: ", current_state_prediction)
            #    print("batch action: ", actions)
            #    print("batch next_states: ", next_states)

            # run the network using next_states from the memory pool entry.  The outputs will be used for the cost function
            # next_state_prediction = qnn.model.predict(np.array(next_states).reshape([BATCH_SIZE, NUM_INPUTS]), batch_size=BATCH_SIZE)
            # here is the key difference with double q learning
            # we get the output for next_state from the shadow network instead of the self.model network
            # The shadow network is more stable, as it changes much more slowly than the self.model network
            next_state_prediction = qnn.shadow_network.predict(np.array(next_states).reshape([BATCH_SIZE, NUM_INPUTS]), batch_size=BATCH_SIZE)
            # next_state_prediction is a BATCH_SIZE list, each element is a list of 4 elements-- the predicted q table values.   However, only the max valued element has any use,
            # as it is the expected value of the next state and its index corresponds to the best action to take from the next state

            # create an expected total reward to send to the network for training
            # the network will compare the values in q_labels to the output values it generates from the input state
            # and calculate the loss (mean squared error)
            # and use that loss with the optimizer to back propagate and modify the network weights
            q_labels = []
            # q_labels is a BATCH_SIZE list, each element is a list of 4 elements.  See below for this list contents
            # go through each entry in the batch.  This could be done in a fancy pythonic way, but this code is easier to understand
            # future_reward = [] # used in testing to make sure we understand the mse error loss calculation
            # output_value = []  # used in testing to make sure we understand the mse error loss calculation
            for i in range(BATCH_SIZE):
                # create 4 element list for this entry in the batch
                q_label_entry = []
                for j in range(NUM_ACTIONS):
                    # go through all possible actions
                    # For the action that corresponds to the one that was taken in the memory pool entry, we will put the total expected reward into that index
                    # For all the other actions, put the output values from the current state into that index
                    # we do this because the loss function is set to be the standard mean squared error function
                    # which would take the squared difference of each entry.
                    # However, in our case, the entries that do not correspond to the action taken should not contribute to the loss
                    # By setting these entries to be equal to the output of the current state,
                    # we make the difference between them and the output equal to zero
                    # so they do not contribute to the total loss at all
                    # An alternative method would have been to use a custom loss that selected out just the value for the selected action
                    # But custom losses are tricky and this method is comparatively straightforward
                    # find the action that was taken in the memory pool entry
                    if j == actions[i]:
                        # for the selected action, put the total expected reward into that index
                        q_label_entry.append(rewards[i] + (GAMMA * max(next_state_prediction[i])))
                        #future_reward.append(rewards[i] + (GAMMA * max(next_state_prediction[i])))  # used in testing to make sure we understand the mse error loss calculation
                        #output_value.append(current_state_prediction[i][j])  # used in testing to make sure we understand the mse error loss calculation
                        #print("i = ", i, ", j = ", j, ", appending ", rewards[i] + (GAMMA * max(next_state_prediction[i])))
                    else:
                        # for all the other actions, put the output values from the current state into that index
                        # so that the difference between the output and the target values are zero for these indices
                        q_label_entry.append(current_state_prediction[i][j])
                        #print("i = ", i, ", j = ", j, ", appending ", current_state_prediction[i][j])

                # built the expected results for a single batch entry, append it to q_labels
                #print("i = ", i, ", q_label_entry: ", q_label_entry)
                q_labels.append(q_label_entry)

            #if BATCH_SIZE == 1 or game_counter > NUM_OF_GAMES - 3:
            #    for i in range(BATCH_SIZE):
            #        print("current state: ", current_states[i], ", next state: ", next_states[i], ', reward = ', rewards[i], ", action = ", actions[i])
            #        print("current_state_prediction: ", current_state_prediction[i])
            #        print("next_state_prediction (next_state_prediction): ", next_state_prediction[i])
            #        print("expected_value (q_labels): ", q_labels[i])

            # now that we have a batch of current states and expected outputs (q_labels), we are ready to train
            history = qnn.model.fit(np.array(current_states), np.array(q_labels), epochs=NUM_EPOCHS, batch_size=BATCH_SIZE)
            loss =  history.history['loss']

            '''
             # used in testing to make sure we understand the mse error loss calculation
            loss_mse = 0.0
            for i in range(BATCH_SIZE):
                loss_mse += ((output_value[i] - future_reward[i]) * (output_value[i] - future_reward[i])) / 4.0
            loss_mse /= BATCH_SIZE
            print("loss_mse = {0:.3f}, net reported loss = {1:.3f}".format(loss_mse, loss[0]))
            '''

            # sometimes the loss blows up to infinity.  I do not know why
            # This is something I do not understand and need to investigate further.
            if loss[0] > 10.0:
                print("loss is very high.  loss = ", loss[0])
                print_qtable()
                if loss[0] > 1000:
                    print("exiting due to excessive loss")
                    exit(0)
            avg_loss += loss[0]
            c_list.append(loss[0])
            train_counter += 1

            # update the shadow network parameters slowly from the self.model network
            # this works by pairing every target_network trainable_variable (target) with its corresponding primary_network trainable_variable (primary)
            # then updating t to be closer to e (e.g., if TAU = .1, then t = 0.9t + 0.1e
            for shadow, primary in zip(qnn.shadow_network.trainable_variables, qnn.model.trainable_variables):
                shadow.assign(shadow * (1 - TAU) + primary * TAU)

        if game_over:
            if train_counter > 0:
                avg_loss /=  train_counter
            print("Game number {0:1d} over, avg loss = {1:.3f}, eps = {2:.3f}, steps used = {3:1d}".format(game_counter, avg_loss, eps, steps - previous_steps))
            previous_steps = steps
            #with train_writer.as_default():
            #    tf.summary.scalar('reward', reward, step=game_counter)
            #    tf.summary.scalar('avg loss', avg_loss, step=game_counter)
print('Final cost: {0:.2f}'.format(c_list[-1]))

'''
#plot results, note, for this to work, you need to uncomment import matplotlib.pyplot as plt 
plt.figure(figsize=(14,7))
plt.plot(range(len(c_list)),c_list)
plt.xlabel('Games played')
plt.ylabel('Average Loss')
plt.show()

'''

print_qtable()

print("\n\ntry some games")
for i in range(5):
    if i == 0:
        game.board = [0,0,0,0]
    else:
        game.board = [random.randint(0,1),random.randint(0,1), random.randint(0,1), random.randint(0,1)]
    game_over = False
    move_count = 0
    print("\ngame {0:1d}".format(i+1))
    while (not game_over) and move_count < 10:
        move_count += 1
        single_prediction = qnn.model.predict(np.array(game.board).reshape([1,NUM_INPUTS]), batch_size=None)
        action = np.argmax(single_prediction)
        previous_game_board = np.array(game.board)
        reward, game_over = game.play(action)  # note that this updates the game board to the next state as well as returning reward and game_over
        print('board: ', previous_game_board, ", predicted Q values: ", np.array(single_prediction), ", action = ", action, ", reward: ", reward, ", game_over = ", game_over)
exit(0)
