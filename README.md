# A demonstration of deep_learning_q
This is a simple demonstration of deep q networks and double deep q networks.  It is mostly for me to refer back to in a few years when I want to do reinforcement learning but have forgotten the basics.  It would be helpful if you are somewhat familiar with machine learning and tensorflow and have heard of the Bellman equation and Q tables, however, that is not really needed.  The scripts are heavily commented; enough to be understood on their own I hope.


This README is now updated as a [google doc](https://docs.google.com/document/d/1u3nDfJQA0dOHbSVN1b-hbk5DimEjpF8mFqWq5TAohUM/edit?usp=sharing)


I used these resources to come up to speed and found them very helpful:

https://www.pyimagesearch.com/2019/12/09/how-to-install-tensorflow-2-0-on-ubuntu/

https://arxiv.org/pdf/1509.06461.pdf

https://towardsdatascience.com/qrash-course-deep-q-networks-from-the-ground-up-1bbda41d3677

https://adventuresinmachinelearning.com/reinforcement-learning-tensorflow/

https://adventuresinmachinelearning.com/double-q-reinforcement-learning-in-tensorflow-2/

https://github.com/adventuresinML/adventures-in-ml-code/blob/master/double_q_tensorflow2.py


### to use:
install tensorflow 2, numpy, and python 3.6

For deep q learning, run this script with python 3.6: `scripts/qnet2_demo.py`

For double deep q learning, run this one: `scripts/qnet2_double_demo.py`

For double deep q learning, packaged into a class to make it easy to import into other scripts, run this one `qnet2_double_class_demo.py`

### Q learning:
#### Bellman equation:

```
Q(s,a) = r(s,a) + (max over a):  (gamma * Q(s’, a))
Where Q is a table with rows as actions and columns as states.
a is the action.
s is the current state
s' is the new state achieved from the action.
r is the reward received from taking the action a in state s.
gamma is the discount factor that balances the effect of long-ago rewards vs recent rewards.
```
This is poor notation because it implies that the second term on the right has some dependence on the value of `a` on the left but it does not.  The value is maximized over all the actions in the new state `s'`.  It has nothing to do with the action picked from the original state `s`.  Better notation is:

`Q(s,a) = r(s,a) + (max over a’):  (gamma * Q(s’,a’))`

##### Example of Q learning without networks:

The `Q table` is a matrix with rows corresponding to possible actions and columns corresponding to possible states.  Start with all entries = `0`. Take a random action, submit that to the game you are playing, get back a reward and a new state. Look at the column in the Q table that corresponds to the new state.  Find the maximum value in that column (it will be 0 since this is the first step and all the entries are 0). That value corresponds to the best action we can take from that state and is also the max expected future reward for that state.  Multiply that reward by `gamma` and add the product to the reward we got for our action.  Put the sum into the Q table entry that corresponds to the initial state and the action we took. Now our Q table has one entry that is non-zero (assuming the reward was not 0).

Now, in the new state, find the max value in the column and do the corresponding action.  Get a reward and a new state. Apply the previous method.

Iterate until the table converges or the game ends.

This approach is greedy, in that it always picks the “best” answer.  That can lead to places in the Q table that rarely get explored, resulting in some non-optimum entries unless you do a huge number of iterations.  A way around this is to occasionally choose an answer other than the “best” one. That approache is typically implemented by using a parameter `epsilon`. When a random number between 0 and 1 is less than `epsilon`, then we choose a random action rather than following the Q table max action.

Thinking about it, it may be a good idea to do a lot of random actions at the start.  This gets the Q table nicely explored and updated before beginning to find the paths that lead to goals.  It may save time overall because taking random actions is fast. You do not have to look up the Q table values for the current state and choose the “best” one.  You only have to look up the next state Q values so that you know how to update the Q value corresponding to the current state and random action. This means that each step only requires a single Q table lookup rather than two.

### Deep Q learning:

Use a network that takes in the state and outputs a predicted Q value for each possible action.  In other words, the output corresponds to the column of the Q table for that state. To use a network, we need a cost function to minimize.  Our goal is to obtain a stable Q table represented by the outputs of the network for each input state. Looking at the Bellman equation,

`Q(s,a) = r(s,a) + (max over a’):  (gamma * Q(s’,a’))`

when this is true for all states and actions, we have a stable Q table. Our objective is to minimize

`Q(s,a) - (r(s,a) + (max over a’):  (gamma * Q(s’,a’)))`

But our cost function should be non-negative, so we will square the value:

`cost function = (Q(s,a) - (r(s,a) + (max over a’):  (gamma * Q(s’,a’)))) ^ 2`

In the network context, this corresponds to the mean squared error cost function, where the current Q value is the “prediction” or “y” and the immediate plus future expected rewards are the “target” or y’ in the equation

`loss = 1/n * sum( (y(i) - y’(i)) ^ 2 )`

To minimize the cost function, networks use back-propagation to modify the weights.  Back-propagation relies on the partial derivatives of the cost function for each weight in the network.  The concept is to find the gradient of the cost function as a vector sum of all the partial derivatives of the weights with respect to the cost function.  Then modify the weights to maximize the gradient, so that the network moves in the fastest direction toward a minimum of the cost function. That implies that the cost function has to be differentiable and I don’t understand how a function that pulls out a max value can be smooth.  This is something to learn more about.

Next we have a training scheme. Choose some network architecture and initial weights. The input to the network is the current state.  The desired output from the network are the Q table entries corresponding to the input state. Put a few hidden layers between the input and the output layers.

Now provide a state as the initial input.  Take the output of the network and find the one with the largest value.  The index of that output is the chosen action. Apply that action to get a reward and the next state.  Provide that next state to the network and get the output. We use the output from the next state to calculate the cost function, just as we did with the Q table:

`cost function = reward + gamma * (max over a’)Q(s’,a’)`

In other words, find the largest value in the output from the next state.  Multiply that by `gamma`. Add the reward to that product to get a total expected reward.  If we were doing Q tables, then we would put that total expected reward into the Q table entry for the initial state and action.  With a network, we will take the difference between the total expected reward and the largest value we got as output from the initial input, square it, and that is the value of our cost function for that step.  We then do network training, where back-propagation uses the cost function value to modify the weights in a direction that will maximally reduce the cost function. Then use the next state as input to the network and iterate.

That scheme will work, but has two big drawbacks.  First, doing a single input at a time is very slow.  We can speed things up considerably by using batches of inputs.  Second, choosing the “best” action each time leads to the same problems we encountered with the greedy Q tables approach.  We need to choose some random actions so that the Q space gets explored more completely and we do not get stuck in loops, just going back and forth between two states.

To train in batches, we use a memory pool that has stored results from the past.  Each memory entry will have the 5 elements that are calculated at each step. Specifically,  `[current state, action, reward, next state, done]`, where `done` is `True` if we reached the end goal. We specify a batch size (typical is 32) and randomly pull that number of memory entries from the pool.  By randomly picking from the pool, we get both recent and old entries. Note that we use old entries, where the next state was chosen using network weights much different than the current network weights.  That is really just a way to explore more of the Q space than if we just used recent entries. I think it is similar to choosing random actions, but not quite as extreme.

Think of the cost function as a landscape with hills and valleys.  You want to find the lowest valley, but our gradient descent algorithm will take us to the bottom of the valley we are currently in, not to the bottom of the deepest valley.  Using random actions can pop us out of our current valley to anywhere in the landscape. I think that using old memory entries is more like popping us out of our current valley into another nearby valley.  In other words, we move away from our local minimum, but not as far away as random actions would take us.

Ideally, we would use just random actions until the entire landscape was explored and then would have a fully filled out Q space and would know how to get the absolute minimum cost function. Note that random actions are fast because you only have to run the network once for each step.  Normally, the network is run for the current state and the output used to pick the next state, then it is run for the next state and the output used to get the cost function (along with the reward), but when you pick a random action, then you don’t have to run the network for the current state, so the whole step is nearly twice as fast as a regular step).   I think the reason we don’t do that is because the Q space is too large in real problems and it would take far too long to randomly explore it all, even though random exploration is nearly twice as fast as regular steps. Instead, we do some initial random exploration and, based on that, go down into the best valley we found. Occasionally we use some old memory entries and some random actions just to see if we can pop out into a better valley, but generally we just head on down to the local valley minimum.  This is another area where I am unsure and need to learn more.

### Double deep Q learning.

One weakness of deep Q learning comes from using the network to both generate the Q table values as outputs and to generate the next state outputs that are used for the cost function.  Since the network changes with each step, the cost function is a moving target and may not converge very well. A way to make things more stable is to use a second network (I call it a "shadow" network) that changes more slowly than the primary network.  The next state is fed into the shadow network to get outputs for the cost function. The shadow network is not trained, it is basically a memory of what the primary network was in the past.
Implementing double deep Q learning is easy.  The shadow network is created with the same architecture and same initialization as the original network.  During each step, the shadow network takes in the next state and its output is used for the cost function calculation.
At the end of each step, the weights of the shadow network are adjusted to move a bit toward the weights of the primary network.

### Here is an example.

We will learn to play a game where the input is a list of four numbers, each number is either 0 or 1.

The goal is to get to the state [1,1,1,1].

An action is to toggle an entry in the list, from 0 to 1 or 1 to 0.

The game has 16 states: [0,0,0,0], [0,0,0,1], … [1,1,1,1]

and 4 actions (toggle entry 1, toggle entry 2, toggle entry 3, toggle entry 4).

Changing a 0 to a 1 gets a +1 reward. Changing a 1 to a 0 gets a -1 reward.

When the state reaches [1,1,1,1] the game is done.

Our network layers are as follows:
```
input layer:  four nodes, one for each entry in the list
first hidden layer:  32 nodes, each attached to all 4 input nodes
second hidden layer: 32 nodes each attached to all 32 nodes of the first hidden layer
output layer: 4 nodes, each attached to all 32 nodes of the first layer.
```

We use a mean squared error cost function.  This is important. See below how we arrange for the input state and expected output state for training to utilize that cost function.

We randomly initialize all the network weights and use relu activation on the hidden layers and use a standard gradient descent approach to drive back-propagation and minimize the cost function.

We initialize the memory pool with 96 entries (3 * BATCH_SIZE) using random actions before doing any training.  Each entry in the memory pool has 5 elements (input state, action, reward, new state, done)

We are ready to start training.  Even after initializing the memory pool, we still choose a lot of random actions and gradually choose more and more directed actions.  We use a decaying exponential to specify the probability of choosing a random action.

Take BATCH_SIZE (32) entries from the memory pool, run the network and, for each entry:

    1. Get the output, a list of 4 numbers that represent the Q table entries for the input state.

    2. Find the max value in that list and get its index.  The index corresponds to an action.

    3. Apply that action to the game and get back a reward and also a next state.

    4. Run the network again, using the next state as input, and get the output for the next state.  For double deep q learning, run the shadow network here instead of the main network.

    5. Find the max value in the output list.  Multiply it by GAMMA.

    6. Add that product to the reward to get the expected total reward.

    7. Create an expected output list with 4 entries.

    8. For the index corresponding to the index of the original output max value, put the expected total reward

    9. For the other three indexes, put the same values as in the original output list

    10. Now the squared error between the original output list and the expected output list is just the squared difference between the original output max value and the expected total reward

    11. Train the network, using the input state and the expected output list.  Note that during training, the input state will produce an output state that is the same as we got above.  That output state will be compared to the expected output list, which is our expected total reward, to compute the cost function (mean squared error).  We have set it up so that this cost function corresponds to the square of the expected total reward.
    
    12. For double deep q learning, update the shadow network weights to be a bit closer to the main network weights.

Next do these steps a bunch of times (reasonable to do them BATCH_SIZE times):

    1. Get the next action either randomly or by running the network with the current state and using the output to pick an action.

    2. Run the game to get the usual list of 5 elements (input state, action, reward, new state, done).

    3. If the game is done, reset the game.

    4. Add the list to the memory pool.

Test to see if the game is played correctly from various starting states.

Look at the values of the cost function

Continue to train until it consistently wins and/or the cost function values are sufficiently low.

#### Here is some sample output.
It took about 20 seconds to run on an msi laptop with an NVidia 2060 GPU.

##### Output from qnet2_demo.py

```
Game number 100 over, avg loss = 0.486, eps = 0.570, steps used = 4
Final cost: 0.49
Here are the results for all the states and actions.  This is equivalent to a Q table.
board: [0 0 0 0], predicted Q values: [[1.424995  1.4661819 1.6396253 1.5480971]], best action: 2, correct action? True
board: [0 0 0 1], predicted Q values: [[2.8639898 2.135612  2.788175 1.4746677]], best action: 0, correct action? True
board: [0 0 1 0], predicted Q values: [[2.816849  3.0229337 1.4344662 2.0937896]], best action: 1, correct action? True
board: [0 0 1 1], predicted Q values: [[4.1375937 3.7262971 2.5658875 2.1508937]], best action: 0, correct action? True
board: [0 1 0 0], predicted Q values: [[1.7265869  0.87075955 1.9854214 1.6733687 ]], best action: 2, correct action? True
board: [0 1 0 1], predicted Q values: [[3.2198365 1.7036755 2.9137096 1.4253888]], best action: 0, correct action? True
board: [0 1 1 0], predicted Q values: [[3.0900106 2.1451497 1.694822  2.571436 ]], best action: 0, correct action? True
board: [0 1 1 1], predicted Q values: [[4.3359528 2.8594782 2.697694  2.232774 ]], best action: 0, correct action? True
board: [1 0 0 0], predicted Q values: [[0.8566337 2.7854548 3.6983411 2.773141 ]], best action: 2, correct action? True
board: [1 0 0 1], predicted Q values: [[2.0283914 3.1270604 3.8956676 2.557578 ]], best action: 2, correct action? True
board: [1 0 1 0], predicted Q values: [[1.8592899 3.6936424 2.8056464 3.0050895]], best action: 1, correct action? True
board: [1 0 1 1], predicted Q values: [[3.0232646 4.093118  3.1917794 2.5877442]], best action: 1, correct action? True
board: [1 1 0 0], predicted Q values: [[1.0534071 1.6140449 3.212953  3.1137795]], best action: 2, correct action? True
board: [1 1 0 1], predicted Q values: [[2.496565  2.4236634 3.6384726 2.6402783]], best action: 2, correct action? True
board: [1 1 1 0], predicted Q values: [[1.9071338 2.4991424 2.4875004 3.2343178]], best action: 3, correct action? True

try some games

game 1
board:  [0 0 0 0] , predicted Q values:  [[1.424995 1.4661819 1.6396253 1.5480971]] , action =  2 , reward: 1 , game_over = False
board:  [0 0 1 0] , predicted Q values:  [[2.816849 3.0229337 1.4344662 2.0937896]] , action =  1 , reward: 1 , game_over = False
board:  [0 1 1 0] , predicted Q values:  [[3.0900106 2.1451497 1.694822 2.571436 ]] , action =  0 , reward: 1 , game_over = False
board:  [1 1 1 0] , predicted Q values:  [[1.9071338 2.4991424 2.4875004 3.2343178]] , action =  3 , reward: 1 , game_over = True

game 2
board:  [0 1 0 1] , predicted Q values:  [[3.2198365 1.7036755 2.9137096 1.4253888]] , action =  0 , reward: 1 , game_over = False
board:  [1 1 0 1] , predicted Q values:  [[2.496565 2.4236634 3.6384726 2.6402783]] , action =  2 , reward: 1 , game_over = True

game 3
board:  [0 0 0 0] , predicted Q values:  [[1.424995 1.4661819 1.6396253 1.5480971]] , action =  2 , reward: 1 , game_over = False
board:  [0 0 1 0] , predicted Q values:  [[2.816849 3.0229337 1.4344662 2.0937896]] , action =  1 , reward: 1 , game_over = False
board:  [0 1 1 0] , predicted Q values:  [[3.0900106 2.1451497 1.694822 2.571436 ]] , action =  0 , reward: 1 , game_over = False
board:  [1 1 1 0] , predicted Q values:  [[1.9071338 2.4991424 2.4875004 3.2343178]] , action =  3 , reward: 1 , game_over = True

game 4
board:  [0 1 1 0] , predicted Q values:  [[3.0900106 2.1451497 1.694822 2.571436 ]] , action =  0 , reward: 1 , game_over = False
board:  [1 1 1 0] , predicted Q values:  [[1.9071338 2.4991424 2.4875004 3.2343178]] , action =  3 , reward: 1 , game_over = True

game 5
board:  [1 0 1 0] , predicted Q values:  [[1.8592899 3.6936424 2.8056464 3.0050895]] , action =  1 , reward: 1 , game_over = False
board:  [1 1 1 0] , predicted Q values:  [[1.9071338 2.4991424 2.4875004 3.2343178]] , action =  3 , reward: 1 , game_over = True

```

##### Output from qnet2_double_demo.py

```
Game number 100 over, avg loss = 0.425, eps = 0.610, steps used = 6
Final cost: 0.43
Here are the results for all the states and actions.  This is equivalent to a Q table.
board: [0 0 0 0], predicted Q values: [[0.64071596 0.42053652 0.9132271  0.7513609 ]], best action: 2, correct action? True
board: [0 0 0 1], predicted Q values: [[1.1220925  1.1904682  0.993421   0.15600927]], best action: 1, correct action? True
board: [0 0 1 0], predicted Q values: [[1.1533525  1.5569174  0.10149638 1.2663138 ]], best action: 1, correct action? True
board: [0 0 1 1], predicted Q values: [[1.8311161 2.0300882 0.2055976 0.6402106]], best action: 1, correct action? True
board: [0 1 0 0], predicted Q values: [[ 1.7824448  -0.17638893  1.5687985   1.27246   ]], best action: 0, correct action? True
board: [0 1 0 1], predicted Q values: [[2.2071185 0.1937245 1.5573181 0.7693756]], best action: 0, correct action? True
board: [0 1 1 0], predicted Q values: [[2.192039  0.3259921 0.6513197 1.8153627]], best action: 0, correct action? True
board: [0 1 1 1], predicted Q values: [[2.649065   0.90736324 0.6938875  1.3397164 ]], best action: 0, correct action? True
board: [1 0 0 0], predicted Q values: [[-0.23865443  1.4204998   2.0952282   1.6611402 ]], best action: 2, correct action? True
board: [1 0 0 1], predicted Q values: [[0.29303235 2.1001005  2.2327235  0.75878054]], best action: 2, correct action? True
board: [1 0 1 0], predicted Q values: [[0.22554779 2.296635   1.2069507  2.2381022 ]], best action: 1, correct action? True
board: [1 0 1 1], predicted Q values: [[1.1087738 2.7802408 1.0614178 1.1186368]], best action: 1, correct action? True
board: [1 1 0 0], predicted Q values: [[0.5605383 0.5718007 2.5564728 2.0062387]], best action: 2, correct action? True
board: [1 1 0 1], predicted Q values: [[1.1584196 1.2877352 2.4100177 1.3386865]], best action: 2, correct action? True
board: [1 1 1 0], predicted Q values: [[1.1214242 1.3296864 1.3745363 2.5513103]], best action: 3, correct action? True


try some games

game 1
board:  [0 0 0 0] , predicted Q values:  [[0.64071596 0.42053652 0.9132271  0.7513609 ]] , action =  2 , reward:  1 , game_over =  False
board:  [0 0 1 0] , predicted Q values:  [[1.1533525  1.5569174  0.10149638 1.2663138 ]] , action =  1 , reward:  1 , game_over =  False
board:  [0 1 1 0] , predicted Q values:  [[2.192039  0.3259921 0.6513197 1.8153627]] , action =  0 , reward:  1 , game_over =  False
board:  [1 1 1 0] , predicted Q values:  [[1.1214242 1.3296864 1.3745363 2.5513103]] , action =  3 , reward:  1 , game_over =  True

game 2
board:  [1 0 1 0] , predicted Q values:  [[0.22554779 2.296635   1.2069507  2.2381022 ]] , action =  1 , reward:  1 , game_over =  False
board:  [1 1 1 0] , predicted Q values:  [[1.1214242 1.3296864 1.3745363 2.5513103]] , action =  3 , reward:  1 , game_over =  True

game 3
board:  [0 0 0 0] , predicted Q values:  [[0.64071596 0.42053652 0.9132271  0.7513609 ]] , action =  2 , reward:  1 , game_over =  False
board:  [0 0 1 0] , predicted Q values:  [[1.1533525  1.5569174  0.10149638 1.2663138 ]] , action =  1 , reward:  1 , game_over =  False
board:  [0 1 1 0] , predicted Q values:  [[2.192039  0.3259921 0.6513197 1.8153627]] , action =  0 , reward:  1 , game_over =  False
board:  [1 1 1 0] , predicted Q values:  [[1.1214242 1.3296864 1.3745363 2.5513103]] , action =  3 , reward:  1 , game_over =  True

game 4
board:  [1 1 0 1] , predicted Q values:  [[1.1584196 1.2877352 2.4100177 1.3386865]] , action =  2 , reward:  1 , game_over =  True

game 5
board:  [1 0 1 1] , predicted Q values:  [[1.1087738 2.7802408 1.0614178 1.1186368]] , action =  1 , reward:  1 , game_over =  True
```


